<?php

namespace App\Form;

use App\Entity\CategoryFiles;
use App\Entity\Files;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Files1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('url', FileType::class, array('data_class' => null))
            ->add('created_at', DateTimeType::class, array(
                'data' => new \DateTime()
            ))
            ->add('category', EntityType::class, array(
                'class' => CategoryFiles::class,
                'label' => 'Catégories',
                'expanded' => false
            ))
            ->add('user', EntityType::class, array(
                'class' => User::class,
                'label' => 'Clients',
                'expanded' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Files::class,
        ]);
    }
}
