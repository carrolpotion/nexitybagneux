<?php

namespace App\Controller;

use App\Form\Article1Type;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Entity\Image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{

    /**
     * @Route("/", name="article_index", methods={"GET"})
     */
    public function article()
    {
        
        $article=$this->getDoctrine()
        ->getRepository('App:Article')
        ->findByArticle()
    ;
        return $this->render('article/index.html.twig', [
            'articles' => $article,
        ]);
    }

    /**
     * @Route("/new", name="article_new")
     * @param Request $request
     * @return Response
     * @throws \Exception
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function new(Request $request): Response
    {
        $article = new Article();
        $form = $this->createForm(Article1Type::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute('article_index');
        }
      
        return $this->render('article/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

        /**
     * @Route("/{id}", name="article_show", methods={"GET"})
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="article_edit", methods={"GET","POST"})
     *@IsGranted({"ROLE_ADMIN"})
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(Article1Type::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('article_index', [
                'id' => $article->getId(),
            ]);
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="article_delete")
     * @param ArticleRepository $articleRepository
     * @param int $id
     * @return RedirectResponse
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function delete(ArticleRepository $articleRepository, int $id): RedirectResponse
    {
        $article = $articleRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        foreach ($article->getImages() as $image) {
            $article->removeImage($image);
        }
        $em->remove($article);
        $em->flush();
        return $this->redirectToRoute('article_index');
    }

}
