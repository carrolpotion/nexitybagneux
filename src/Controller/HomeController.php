<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig');
    }

        /**
     * @Route("/error", name="404")
     */
    public function error()
    {
        return $this->render('home/error404.html.twig');
    }

    /**
     * @Route("/quartier", name="quartier")
     */
    public function quartier()
    {
        return $this->render('home/quartier.html.twig');
    }

    /**
     * @Route("/projet", name="projet")
     */
    public function projet()
    {
        return $this->render('home/projet.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        return $this->render('home/contact.html.twig');
    }
}
