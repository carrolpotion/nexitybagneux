<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Article;
use App\Entity\Image;

class ImageController extends AbstractController
{
    /**
     * @Route("/image", name="image")
     */
    public function index()
    {
        $listArticle=$this->getDoctrine()
        ->getManager()
        ->getRepository('App:Article')
        ->findByArticle()
    ;

     return $this->render('image/index.html.twig', array(
         'listArticle'  =>$listArticle,
    ));
    }
    
    /**
     * @Route("/image/supprimer/{id}", name="admin_delete_image")
     * @param int $id
     * @return Response
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function delete(int $id): Response
    {
        // get id for image
        $image = $this->getDoctrine()->getRepository(Image::class)->find($id);
        $article = $image->getArticle();
        // remove and flush bdd
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();
        return $this->redirectToRoute('article_edit', ['id' => $article->getId()]);
    }
}