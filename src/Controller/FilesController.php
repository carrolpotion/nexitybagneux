<?php

namespace App\Controller;

use App\Entity\Files;
use App\Form\Files1Type;
use App\Repository\FilesRepository;
use App\Repository\CategoryFilesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/files")
 */
class FilesController extends AbstractController
{

    /**
     * @Route("/", name="files_index", methods={"GET"})
     */
    public function index(FilesRepository $filesRepository)
    {
        $em = $this->getDoctrine()->getManager();

        $categoryFiles = $em->getRepository('App:CategoryFiles')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $files = $this->getDoctrine()
                ->getRepository('App:Files')
                ->findBy(array('user' => $user->getId()));

        return $this->render('files/index.html.twig', array(
                    'categoryFiles' => $categoryFiles,
                    'user' => $user,
                    'files' => $files,
                ));
    }

    /**
     * @Route("/all", name="files_all", methods={"GET"})
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function All(FilesRepository $filesRepository): Response
    {
        return $this->render('files/all.html.twig', [
            'files' => $filesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="files_new", methods={"GET","POST"})
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function new(Request $request): Response
    {
        $file = new Files();
        $form = $this->createForm(Files1Type::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file->upload();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($file);
            $entityManager->flush();

            return $this->redirectToRoute('files_all');
        }

        return $this->render('files/new.html.twig', [
            'file' => $file,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="files_show", methods={"GET"})
     */
    public function show(Files $file): Response
    {
        return $this->render('files/show.html.twig', [
            'file' => $file,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="files_edit", methods={"GET","POST"})
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function edit(Request $request, Files $file): Response
    {
        $form = $this->createForm(Files1Type::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file->upload();

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('files_all', [
                'id' => $file->getId(),
            ]);
        }

        return $this->render('files/edit.html.twig', [
            'file' => $file,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="files_delete", methods={"DELETE"})
     * @IsGranted({"ROLE_ADMIN"})
     */
    public function delete(Request $request, Files $file): Response
    {
        if ($this->isCsrfTokenValid('delete'.$file->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($file);
            $entityManager->flush();
        }

        return $this->redirectToRoute('files_all');
    }
}
