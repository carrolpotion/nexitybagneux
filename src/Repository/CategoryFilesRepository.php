<?php

namespace App\Repository;

use App\Entity\CategoryFiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CategoryFiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryFiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryFiles[]    findAll()
 * @method CategoryFiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryFilesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoryFiles::class);
    }

    public function findById()
    {
        $query = $this->createQueryBuilder('a')
            ->orderBy('a.id')
            ->getQuery()
            ->getResult()
        ;
        return $query;
    }

    // /**
    //  * @return CategoryFiles[] Returns an array of CategoryFiles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryFiles
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
