-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 18 juil. 2019 à 22:46
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bagneuxnexity`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `date` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `titre`, `content`, `date`, `updated_at`) VALUES
(16, 'Mai 2019', 'Amicitiae societate generis aut natura humani res inter quod ipsa potest humani omnis duos \r\nita aut autem natura ex infinita ipsa est ut quod iungeretur iungeretur maxime ex aut duos caritas paucos amicitiae\r\ncontracta quam hoc ex societate est aut.', '2019-07-17 21:05:44', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `category_files`
--

DROP TABLE IF EXISTS `category_files`;
CREATE TABLE IF NOT EXISTS `category_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category_files`
--

INSERT INTO `category_files` (`id`, `name`, `picture`, `updated_at`) VALUES
(1, 'Mes attestations travaux', '1.jpg', NULL),
(2, 'Mes appels de fonds', '2.jpg', NULL),
(3, 'Le plan masse', '3.jpg', NULL),
(4, 'Les plans de mon logement', '4.png', NULL),
(5, 'AUTRES ELEMENTS', '5.png', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_635405912469DE2` (`category_id`),
  KEY `IDX_6354059A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `files`
--

INSERT INTO `files` (`id`, `category_id`, `user_id`, `name`, `url`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'test doc admin', 'TEXTES_BALMA_201903.pdf', '2019-07-09 00:35:00', NULL),
(3, 3, 2, 'TEST DOC USER', 'TEXTES_CAEN_201904.pdf', '2019-07-09 00:48:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C53D045F7294869C` (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `article_id`, `url`, `alt`) VALUES
(6, NULL, '20100213_REP_MONTAIGNE_©EliseRobagliaIMG_9472.jpg', NULL),
(7, NULL, 'NEXITY_BAGNEUX_20180927©KatiaPeignoux_IMG_8194.jpg', NULL),
(8, NULL, 'NEXITY_BAGNEUX_20180927©KatiaPeignoux_IMG_8195.jpg', NULL),
(9, NULL, 'NEXITY_BAGNEUX_20180927©KatiaPeignoux_IMG_8197.jpg', NULL),
(10, NULL, 'NEXITY_BAGNEUX_20180927©KatiaPeignoux_Z78_2259.jpg', NULL),
(11, NULL, 'NEXITY_BAGNEUX_20180927©KatiaPeignoux_Z78_2270.jpg', NULL),
(15, NULL, '20181023_NEXITY_BAGNEUX_©EliseRobagliaDJI_0061.jpg', NULL),
(16, NULL, 'NEXITY_BAGNEUX_20181114©KatiaPeignoux_K67_0548.jpg', NULL),
(17, NULL, 'NEXITY_BAGNEUX_20181114©KatiaPeignoux_K67_0561.jpg', NULL),
(18, NULL, 'NEXITY_BAGNEUX_20181114©KatiaPeignoux_K67_0565.jpg', NULL),
(19, NULL, 'NEXITY_BAGNEUX_20181114©KatiaPeignoux_K67_0567.jpg', NULL),
(20, NULL, 'NEXITY_BAGNEUX_20181114©KatiaPeignoux_K67_0568.jpg', NULL),
(21, 16, 'NEXITY_BAGNEUX_20190507©KatiaPeignoux_IMG_6438.jpg', NULL),
(22, 16, 'NEXITY_BAGNEUX_20190507©KatiaPeignoux_IMG_6446.jpg', NULL),
(23, 16, 'NEXITY_BAGNEUX_20190507©KatiaPeignoux_IMG_6451.jpg', NULL),
(24, 16, 'NEXITY_BAGNEUX_20190507©KatiaPeignoux_IMG_6454.jpg', NULL),
(25, 16, 'NEXITY_BAGNEUX_20190507©KatiaPeignoux_IMG_6458.jpg', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190708131940', '2019-07-08 13:26:51'),
('20190708165535', '2019-07-08 16:56:25'),
('20190708173139', '2019-07-08 17:31:49'),
('20190708175657', '2019-07-08 17:57:10');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `password`, `roles`, `username`) VALUES
(1, 'dev', 'dev', '$argon2id$v=19$m=65536,t=6,p=1$I0m3r5pOt07NCbQdHqy+8A$OH77AziR7cNoOGSjb6I8X33mgM2Z0Yh4Zb9jOFOcBYs', 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 'dev'),
(2, 'user', 'user', '$argon2id$v=19$m=65536,t=6,p=1$96XO+X4PVV7tkUS7hAs0Ww$B6M666p380WUtDfnqKc0iVI3qXdRdy4Qy5ZOgj1WmAE', 'a:1:{i:0;s:9:\"ROLE_USER\";}', 'user');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `FK_635405912469DE2` FOREIGN KEY (`category_id`) REFERENCES `category_files` (`id`),
  ADD CONSTRAINT `FK_6354059A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045F7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
